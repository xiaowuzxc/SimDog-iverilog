"""
本代码由[Tkinter布局助手]生成
官网:https://www.pytk.net/tkinter-helper
QQ交流群:788392508
"""
from tkinter import *
from tkinter.ttk import *
from typing import Dict
class WinGUI(Tk):
    widget_dic: Dict[str, Widget] = {}
    复选框val=0#给外面的函数用
    def __init__(self):
        super().__init__()
        self.__win()
        self.widget_dic["tk_input_单行输入输出"] = self.__tk_input_单行输入输出(self)
        self.widget_dic["tk_label_输入区域"] = self.__tk_label_输入区域(self)
        self.widget_dic["tk_button_按键0"] = self.__tk_button_按键0(self)
        self.widget_dic["tk_check_button_复选框0"] = self.__tk_check_button_复选框0(self)[0]
        self.复选框val = self.__tk_check_button_复选框0(self)[1]#绑定量给复选框val
        self.widget_dic["tk_select_box_下拉框"] = self.__tk_select_box_下拉框(self)
        self.widget_dic["tk_list_box_列表框"] = self.__tk_list_box_列表框(self)
        self.widget_dic["tk_button_ljp7senv"] = self.__tk_button_ljp7senv(self)
        self.widget_dic["tk_button_ljp7sfwz"] = self.__tk_button_ljp7sfwz(self)
    def __win(self):
        self.title("Tkinter布局助手")
        # 设置窗口大小、居中
        width = 600
        height = 500
        screenwidth = self.winfo_screenwidth()
        screenheight = self.winfo_screenheight()
        geometry = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.geometry(geometry)
        self.resizable(width=False, height=False)
        # 自动隐藏滚动条
    def scrollbar_autohide(self,bar,widget):
        self.__scrollbar_hide(bar,widget)
        widget.bind("<Enter>", lambda e: self.__scrollbar_show(bar,widget))
        bar.bind("<Enter>", lambda e: self.__scrollbar_show(bar,widget))
        widget.bind("<Leave>", lambda e: self.__scrollbar_hide(bar,widget))
        bar.bind("<Leave>", lambda e: self.__scrollbar_hide(bar,widget))
    
    def __scrollbar_show(self,bar,widget):
        bar.lift(widget)
    def __scrollbar_hide(self,bar,widget):
        bar.lower(widget)
    
    def vbar(self,ele, x, y, w, h, parent):
        sw = 15 # Scrollbar 宽度
        x = x + w - sw
        vbar = Scrollbar(parent)
        ele.configure(yscrollcommand=vbar.set)
        vbar.config(command=ele.yview)
        vbar.place(x=x, y=y, width=sw, height=h)
        self.scrollbar_autohide(vbar,ele)
    def __tk_input_单行输入输出(self,parent):
        ipt = Entry(parent, )
        ipt.place(x=80, y=100, width=150, height=30)
        return ipt
    def __tk_label_输入区域(self,parent):
        label = Label(parent,text="标签",anchor="center", )
        label.place(x=80, y=20, width=50, height=30)
        return label
    def __tk_button_按键0(self,parent):
        btn = Button(parent, text="按钮", takefocus=False,)
        btn.place(x=180, y=60, width=50, height=30)
        return btn
    def __tk_check_button_复选框0(self,parent):
        绑定量=BooleanVar()#tk布尔型
        cb = Checkbutton(parent,text="点一下",variable = 绑定量)#绑定
        cb.place(x=80, y=60, width=80, height=30)
        return cb,绑定量#元组形式返回
    def __tk_select_box_下拉框(self,parent):#下拉框
        cb = Combobox(parent, state="readonly", )
        cb['values'] = ("114","514","1919","810","11451419198100001111123456789123456789")
        cb.current(0)#默认第一项
        cb.place(x=80, y=150, width=153, height=30)
        return cb
    def __tk_list_box_列表框(self,parent):#列表框，添加上下左右滚动条，长字符会遮挡
        lb = Listbox(parent,selectmode="extended")
        lb.place(x=80, y=250, width=200, height=200)
        #x滚动条塞进内部
        sbarx = Scrollbar(lb, orient=HORIZONTAL, command=lb.xview)#x滚动条，父容器lb，水平，和x轴关联
        sbarx.pack(side=BOTTOM, fill=X)#放置位置，side=[TOP,BOTTOM,RIGHT,LEFT]
        lb.configure(xscrollcommand=sbarx.set)#x扔进去
        #y滚动条放外面
        sbary = Scrollbar(parent)#进父容器
        lb.configure(yscrollcommand=sbary.set)#放y轴
        sbary.place(x=280, y=250, width=20, height=200)#放在x=x+width(lb)，sbary.width=20比较合适
        sbary.config(command=lb.yview)#和y轴关联

        
        return lb
    def __tk_button_ljp7senv(self,parent):
        btn = Button(parent, text="添加", takefocus=False,)
        btn.place(x=250, y=150, width=50, height=30)
        return btn
    def __tk_button_ljp7sfwz(self,parent):
        btn = Button(parent, text="删除", takefocus=False,)
        btn.place(x=250, y=190, width=50, height=30)
        return btn
class Win(WinGUI):
    def __init__(self):
        super().__init__()
        self.__event_bind()

    def 按下按键(self,evt):
        #读复选框
        print(type(self.复选框val))
        复选框=self.复选框val.get()#通过X.get读取复选框
        print("<Button>事件:点一下状态:",复选框)#通过X.get读取复选框
        #读写Entry文本框
        print("文本框的值:",self.widget_dic["tk_input_单行输入输出"].get())#读
        self.widget_dic["tk_input_单行输入输出"].delete(0, END)#清除
        self.widget_dic["tk_input_单行输入输出"].insert(0, "114"+str(复选框))#写入

    def 添加列表项(self,evt):
        待添加的列表项=self.widget_dic["tk_select_box_下拉框"].get()#读取下拉框
        self.widget_dic["tk_list_box_列表框"].insert(END,待添加的列表项)#插入列表最后

    def 删除列表项(self,evt):
        列表长度=self.widget_dic["tk_list_box_列表框"].size()
        print("列表长度:",列表长度)
        for i in range(列表长度):#遍历
            if self.widget_dic["tk_list_box_列表框"].selection_includes(i):#若选中
                self.widget_dic["tk_list_box_列表框"].delete(i)#删除


    def __event_bind(self):
        self.widget_dic["tk_button_按键0"].bind('<Button>',self.按下按键)
        self.widget_dic["tk_button_ljp7senv"].bind('<Button>',self.添加列表项)
        self.widget_dic["tk_button_ljp7sfwz"].bind('<Button>',self.删除列表项)
        pass
if __name__ == "__main__":
    win = Win()
    win.mainloop()

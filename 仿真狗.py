"""
本代码由[Tkinter布局助手]生成
官网:https://www.pytk.net/tkinter-helper
QQ交流群:788392508
"""
from tkinter import *
from tkinter.ttk import *
from typing import Dict
class WinGUI(Tk):
    widget_dic: Dict[str, Widget] = {}
    def __init__(self):
        super().__init__()
        self.__win()
        self.widget_dic["tk_input_工程名称_输入框"] = self.__tk_input_工程名称_输入框(self)
        self.widget_dic["tk_button_设置工程路径_按钮"] = self.__tk_button_设置工程路径_按钮(self)
        self.widget_dic["tk_input_工程路径_显示框"] = self.__tk_input_工程路径_显示框(self)
        self.widget_dic["tk_tabs_ljtdxxwb"] = self.__tk_tabs_ljtdxxwb(self)
        self.widget_dic["tk_list_box_lk7nlddi"] = self.__tk_list_box_lk7nlddi( self.widget_dic["tk_tabs_ljtdxxwb_3"])
        self.widget_dic["tk_button_lk7nm7nw"] = self.__tk_button_lk7nm7nw( self.widget_dic["tk_tabs_ljtdxxwb_3"])
        self.widget_dic["tk_button_lk7nmc59"] = self.__tk_button_lk7nmc59( self.widget_dic["tk_tabs_ljtdxxwb_3"])
        self.widget_dic["tk_list_box_lk7nor1b"] = self.__tk_list_box_lk7nor1b( self.widget_dic["tk_tabs_ljtdxxwb_2"])
        self.widget_dic["tk_list_box_lk7npp9x"] = self.__tk_list_box_lk7npp9x( self.widget_dic["tk_tabs_ljtdxxwb_1"])
        self.widget_dic["tk_list_box_lk7nq0nj"] = self.__tk_list_box_lk7nq0nj( self.widget_dic["tk_tabs_ljtdxxwb_0"])
        self.widget_dic["tk_button_lk7nqj5f"] = self.__tk_button_lk7nqj5f( self.widget_dic["tk_tabs_ljtdxxwb_0"])
        self.widget_dic["tk_button_lk7nqkmb"] = self.__tk_button_lk7nqkmb( self.widget_dic["tk_tabs_ljtdxxwb_0"])
        self.widget_dic["tk_label_lk7nqrh1_左对齐文本"] = self.__tk_label_lk7nqrh1_左对齐文本( self.widget_dic["tk_tabs_ljtdxxwb_0"])
        self.widget_dic["tk_button_lk7nqu8u"] = self.__tk_button_lk7nqu8u( self.widget_dic["tk_tabs_ljtdxxwb_1"])
        self.widget_dic["tk_button_lk7nqwdy"] = self.__tk_button_lk7nqwdy( self.widget_dic["tk_tabs_ljtdxxwb_1"])
        self.widget_dic["tk_label_lk7nqyri_左对齐文本"] = self.__tk_label_lk7nqyri_左对齐文本( self.widget_dic["tk_tabs_ljtdxxwb_1"])
        self.widget_dic["tk_button_lk7nr4r4"] = self.__tk_button_lk7nr4r4( self.widget_dic["tk_tabs_ljtdxxwb_2"])
        self.widget_dic["tk_button_lk7nr648"] = self.__tk_button_lk7nr648( self.widget_dic["tk_tabs_ljtdxxwb_2"])
        self.widget_dic["tk_label_lk7nra3l_左对齐文本"] = self.__tk_label_lk7nra3l_左对齐文本( self.widget_dic["tk_tabs_ljtdxxwb_2"])
        self.widget_dic["tk_button_lk95217r"] = self.__tk_button_lk95217r( self.widget_dic["tk_tabs_ljtdxxwb_4"])
        self.widget_dic["tk_button_lk9526cu"] = self.__tk_button_lk9526cu( self.widget_dic["tk_tabs_ljtdxxwb_4"])
        self.widget_dic["tk_button_lk952787"] = self.__tk_button_lk952787( self.widget_dic["tk_tabs_ljtdxxwb_4"])
        self.widget_dic["tk_label_lk953ttv_左对齐文本"] = self.__tk_label_lk953ttv_左对齐文本( self.widget_dic["tk_tabs_ljtdxxwb_4"])
        self.widget_dic["tk_list_box_lk95514j"] = self.__tk_list_box_lk95514j( self.widget_dic["tk_tabs_ljtdxxwb_4"])
        self.widget_dic["tk_input_lk95739m"] = self.__tk_input_lk95739m( self.widget_dic["tk_tabs_ljtdxxwb_3"])
        self.widget_dic["tk_label_lk95crde_左对齐文本"] = self.__tk_label_lk95crde_左对齐文本( self.widget_dic["tk_tabs_ljtdxxwb_3"])
        self.widget_dic["tk_label_lk95e4m4_左对齐文本"] = self.__tk_label_lk95e4m4_左对齐文本( self.widget_dic["tk_tabs_ljtdxxwb_3"])
        self.widget_dic["tk_label_工程名称"] = self.__tk_label_工程名称(self)
        self.widget_dic["tk_tabs_ljtrmraf"] = self.__tk_tabs_ljtrmraf(self)
        self.widget_dic["tk_text_信息文本框"] = self.__tk_text_信息文本框( self.widget_dic["tk_tabs_ljtrmraf_0"])
        self.widget_dic["tk_list_box_ljts4yka"] = self.__tk_list_box_ljts4yka( self.widget_dic["tk_tabs_ljtrmraf_1"])
        self.widget_dic["tk_list_box_ljttk802"] = self.__tk_list_box_ljttk802( self.widget_dic["tk_tabs_ljtrmraf_2"])
        self.widget_dic["tk_label_frame_ljtrrw77"] = self.__tk_label_frame_ljtrrw77(self)
        self.widget_dic["tk_select_box_lk94nk8u"] = self.__tk_select_box_lk94nk8u( self.widget_dic["tk_label_frame_ljtrrw77"]) 
        self.widget_dic["tk_check_button_lk9fh8v8"] = self.__tk_check_button_lk9fh8v8( self.widget_dic["tk_label_frame_ljtrrw77"]) 
        self.widget_dic["tk_check_button_lk9fkk4k"] = self.__tk_check_button_lk9fkk4k( self.widget_dic["tk_label_frame_ljtrrw77"]) 
        self.widget_dic["tk_label_lk9fmrcl_左对齐文本"] = self.__tk_label_lk9fmrcl_左对齐文本( self.widget_dic["tk_label_frame_ljtrrw77"]) 
        self.widget_dic["tk_input_lk9foc7k"] = self.__tk_input_lk9foc7k( self.widget_dic["tk_label_frame_ljtrrw77"]) 
        self.widget_dic["tk_button_编译iverilog_按钮"] = self.__tk_button_编译iverilog_按钮( self.widget_dic["tk_label_frame_ljtrrw77"]) 
        self.widget_dic["tk_label_lk9g07nn_左对齐文本"] = self.__tk_label_lk9g07nn_左对齐文本( self.widget_dic["tk_label_frame_ljtrrw77"]) 
        self.widget_dic["tk_label_lk9g339b_左对齐文本"] = self.__tk_label_lk9g339b_左对齐文本( self.widget_dic["tk_label_frame_ljtrrw77"]) 
        self.widget_dic["tk_label_frame_ljtrs9ce"] = self.__tk_label_frame_ljtrs9ce(self)
        self.widget_dic["tk_check_button_lk9ftgsl"] = self.__tk_check_button_lk9ftgsl( self.widget_dic["tk_label_frame_ljtrs9ce"]) 
        self.widget_dic["tk_check_button_lk9fvkel"] = self.__tk_check_button_lk9fvkel( self.widget_dic["tk_label_frame_ljtrs9ce"]) 
        self.widget_dic["tk_label_lk9fwn4s_左对齐文本"] = self.__tk_label_lk9fwn4s_左对齐文本( self.widget_dic["tk_label_frame_ljtrs9ce"]) 
        self.widget_dic["tk_select_box_lk9fxdq1"] = self.__tk_select_box_lk9fxdq1( self.widget_dic["tk_label_frame_ljtrs9ce"]) 
        self.widget_dic["tk_input_lk9fysyb"] = self.__tk_input_lk9fysyb( self.widget_dic["tk_label_frame_ljtrs9ce"]) 
        self.widget_dic["tk_label_lk9g5lwz_左对齐文本"] = self.__tk_label_lk9g5lwz_左对齐文本( self.widget_dic["tk_label_frame_ljtrs9ce"]) 
        self.widget_dic["tk_input_lk9g95q4"] = self.__tk_input_lk9g95q4( self.widget_dic["tk_label_frame_ljtrs9ce"]) 
        self.widget_dic["tk_label_lk9g9bhx_左对齐文本"] = self.__tk_label_lk9g9bhx_左对齐文本( self.widget_dic["tk_label_frame_ljtrs9ce"]) 
        self.widget_dic["tk_button_仿真vvp_按钮"] = self.__tk_button_仿真vvp_按钮( self.widget_dic["tk_label_frame_ljtrs9ce"]) 
        self.widget_dic["tk_check_button_lk9hcd50"] = self.__tk_check_button_lk9hcd50( self.widget_dic["tk_label_frame_ljtrs9ce"]) 
        self.widget_dic["tk_button_查看波形_按钮"] = self.__tk_button_查看波形_按钮(self)
        self.widget_dic["tk_button_查看使用教程_按钮"] = self.__tk_button_查看使用教程_按钮(self)
        self.widget_dic["tk_button_清空3个信息框_按钮"] = self.__tk_button_清空3个信息框_按钮(self)
        self.widget_dic["tk_button_打开工程目录_按钮"] = self.__tk_button_打开工程目录_按钮(self)
        self.widget_dic["tk_label_lk8cwqm2"] = self.__tk_label_lk8cwqm2(self)
        self.widget_dic["tk_button_导出脚本_按钮"] = self.__tk_button_导出脚本_按钮(self)
        self.widget_dic["tk_button_lk9gqgjy"] = self.__tk_button_lk9gqgjy(self)
    def __win(self):
        self.title("Tkinter布局助手")
        # 设置窗口大小、居中
        width = 940
        height = 540
        screenwidth = self.winfo_screenwidth()
        screenheight = self.winfo_screenheight()
        geometry = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.geometry(geometry)
        self.resizable(width=False, height=False)
        # 自动隐藏滚动条
    def scrollbar_autohide(self,bar,widget):
        self.__scrollbar_hide(bar,widget)
        widget.bind("<Enter>", lambda e: self.__scrollbar_show(bar,widget))
        bar.bind("<Enter>", lambda e: self.__scrollbar_show(bar,widget))
        widget.bind("<Leave>", lambda e: self.__scrollbar_hide(bar,widget))
        bar.bind("<Leave>", lambda e: self.__scrollbar_hide(bar,widget))
    
    def __scrollbar_show(self,bar,widget):
        bar.lift(widget)
    def __scrollbar_hide(self,bar,widget):
        bar.lower(widget)
    
    def vbar(self,ele, x, y, w, h, parent):
        sw = 15 # Scrollbar 宽度
        x = x + w - sw
        vbar = Scrollbar(parent)
        ele.configure(yscrollcommand=vbar.set)
        vbar.config(command=ele.yview)
        vbar.place(x=x, y=y, width=sw, height=h)
        self.scrollbar_autohide(vbar,ele)
    def __tk_input_工程名称_输入框(self,parent):
        ipt = Entry(parent, )
        ipt.place(x=80, y=10, width=180, height=30)
        return ipt
    def __tk_button_设置工程路径_按钮(self,parent):
        btn = Button(parent, text="设置工程路径", takefocus=False,)
        btn.place(x=270, y=10, width=90, height=30)
        return btn
    def __tk_input_工程路径_显示框(self,parent):
        ipt = Entry(parent, )
        ipt.place(x=80, y=50, width=500, height=30)
        return ipt
    def __tk_tabs_ljtdxxwb(self,parent):
        frame = Notebook(parent)
        self.widget_dic["tk_tabs_ljtdxxwb_0"] = self.__tk_frame_ljtdxxwb_0(frame)
        frame.add(self.widget_dic["tk_tabs_ljtdxxwb_0"], text="源文件列表")
        self.widget_dic["tk_tabs_ljtdxxwb_1"] = self.__tk_frame_ljtdxxwb_1(frame)
        frame.add(self.widget_dic["tk_tabs_ljtdxxwb_1"], text="库模块搜索路径")
        self.widget_dic["tk_tabs_ljtdxxwb_2"] = self.__tk_frame_ljtdxxwb_2(frame)
        frame.add(self.widget_dic["tk_tabs_ljtdxxwb_2"], text="include搜索路径")
        self.widget_dic["tk_tabs_ljtdxxwb_3"] = self.__tk_frame_ljtdxxwb_3(frame)
        frame.add(self.widget_dic["tk_tabs_ljtdxxwb_3"], text="宏定义列表")
        self.widget_dic["tk_tabs_ljtdxxwb_4"] = self.__tk_frame_ljtdxxwb_4(frame)
        frame.add(self.widget_dic["tk_tabs_ljtdxxwb_4"], text="VPI C源文件列表")
        frame.place(x=10, y=90, width=570, height=280)
        return frame
    def __tk_frame_ljtdxxwb_0(self,parent):
        frame = Frame(parent)
        frame.place(x=10, y=90, width=570, height=280)
        return frame
    def __tk_frame_ljtdxxwb_1(self,parent):
        frame = Frame(parent)
        frame.place(x=10, y=90, width=570, height=280)
        return frame
    def __tk_frame_ljtdxxwb_2(self,parent):
        frame = Frame(parent)
        frame.place(x=10, y=90, width=570, height=280)
        return frame
    def __tk_frame_ljtdxxwb_3(self,parent):
        frame = Frame(parent)
        frame.place(x=10, y=90, width=570, height=280)
        return frame
    def __tk_frame_ljtdxxwb_4(self,parent):
        frame = Frame(parent)
        frame.place(x=10, y=90, width=570, height=280)
        return frame
    def __tk_list_box_lk7nlddi(self,parent):
        lb = Listbox(parent)
        
        lb.place(x=0, y=60, width=565, height=190)
        return lb
    def __tk_button_lk7nm7nw(self,parent):
        btn = Button(parent, text="删除", takefocus=False,)
        btn.place(x=500, y=30, width=60, height=30)
        return btn
    def __tk_button_lk7nmc59(self,parent):
        btn = Button(parent, text="添加", takefocus=False,)
        btn.place(x=430, y=30, width=60, height=30)
        return btn
    def __tk_list_box_lk7nor1b(self,parent):
        lb = Listbox(parent)
        
        lb.place(x=0, y=30, width=565, height=220)
        return lb
    def __tk_list_box_lk7npp9x(self,parent):
        lb = Listbox(parent)
        
        lb.place(x=0, y=30, width=565, height=220)
        return lb
    def __tk_list_box_lk7nq0nj(self,parent):
        lb = Listbox(parent)
        
        lb.place(x=0, y=30, width=565, height=220)
        return lb
    def __tk_button_lk7nqj5f(self,parent):
        btn = Button(parent, text="添加", takefocus=False,)
        btn.place(x=430, y=0, width=60, height=30)
        return btn
    def __tk_button_lk7nqkmb(self,parent):
        btn = Button(parent, text="删除", takefocus=False,)
        btn.place(x=500, y=0, width=60, height=30)
        return btn
    def __tk_label_lk7nqrh1_左对齐文本(self,parent):
        label = Label(parent,text="标签",anchor="w", )
        label.place(x=10, y=0, width=50, height=30)
        return label
    def __tk_button_lk7nqu8u(self,parent):
        btn = Button(parent, text="删除", takefocus=False,)
        btn.place(x=500, y=0, width=60, height=30)
        return btn
    def __tk_button_lk7nqwdy(self,parent):
        btn = Button(parent, text="添加", takefocus=False,)
        btn.place(x=430, y=0, width=60, height=30)
        return btn
    def __tk_label_lk7nqyri_左对齐文本(self,parent):
        label = Label(parent,text="标签",anchor="w", )
        label.place(x=10, y=0, width=50, height=30)
        return label
    def __tk_button_lk7nr4r4(self,parent):
        btn = Button(parent, text="删除", takefocus=False,)
        btn.place(x=500, y=0, width=60, height=30)
        return btn
    def __tk_button_lk7nr648(self,parent):
        btn = Button(parent, text="添加", takefocus=False,)
        btn.place(x=430, y=0, width=60, height=30)
        return btn
    def __tk_label_lk7nra3l_左对齐文本(self,parent):
        label = Label(parent,text="标签",anchor="w", )
        label.place(x=10, y=0, width=50, height=30)
        return label
    def __tk_button_lk95217r(self,parent):
        btn = Button(parent, text="删除", takefocus=False,)
        btn.place(x=500, y=0, width=60, height=30)
        return btn
    def __tk_button_lk9526cu(self,parent):
        btn = Button(parent, text="添加", takefocus=False,)
        btn.place(x=430, y=0, width=60, height=30)
        return btn
    def __tk_button_lk952787(self,parent):
        btn = Button(parent, text="编译VPI", takefocus=False,)
        btn.place(x=350, y=0, width=70, height=30)
        return btn
    def __tk_label_lk953ttv_左对齐文本(self,parent):
        label = Label(parent,text="标签",anchor="w", )
        label.place(x=10, y=0, width=50, height=30)
        return label
    def __tk_list_box_lk95514j(self,parent):
        lb = Listbox(parent)
        
        lb.place(x=0, y=30, width=565, height=220)
        return lb
    def __tk_input_lk95739m(self,parent):
        ipt = Entry(parent, )
        ipt.place(x=270, y=30, width=150, height=25)
        return ipt
    def __tk_label_lk95crde_左对齐文本(self,parent):
        label = Label(parent,text="输入并添加宏定义，或选择并删除：",anchor="w", )
        label.place(x=10, y=30, width=260, height=30)
        return label
    def __tk_label_lk95e4m4_左对齐文本(self,parent):
        label = Label(parent,text="请遵从此格式添加宏定义：`define XXX 32 对应 XXX=32 ；   `define YYY 对应 XXX",anchor="w", )
        label.place(x=10, y=0, width=550, height=30)
        return label
    def __tk_label_工程名称(self,parent):
        label = Label(parent,text="工程名称",anchor="center", )
        label.place(x=10, y=10, width=70, height=30)
        return label
    def __tk_tabs_ljtrmraf(self,parent):
        frame = Notebook(parent)
        self.widget_dic["tk_tabs_ljtrmraf_0"] = self.__tk_frame_ljtrmraf_0(frame)
        frame.add(self.widget_dic["tk_tabs_ljtrmraf_0"], text="信息")
        self.widget_dic["tk_tabs_ljtrmraf_1"] = self.__tk_frame_ljtrmraf_1(frame)
        frame.add(self.widget_dic["tk_tabs_ljtrmraf_1"], text="错误")
        self.widget_dic["tk_tabs_ljtrmraf_2"] = self.__tk_frame_ljtrmraf_2(frame)
        frame.add(self.widget_dic["tk_tabs_ljtrmraf_2"], text="警告")
        frame.place(x=10, y=380, width=570, height=160)
        return frame
    def __tk_frame_ljtrmraf_0(self,parent):
        frame = Frame(parent)
        frame.place(x=10, y=380, width=570, height=160)
        return frame
    def __tk_frame_ljtrmraf_1(self,parent):
        frame = Frame(parent)
        frame.place(x=10, y=380, width=570, height=160)
        return frame
    def __tk_frame_ljtrmraf_2(self,parent):
        frame = Frame(parent)
        frame.place(x=10, y=380, width=570, height=160)
        return frame
    def __tk_text_信息文本框(self,parent):
        text = Text(parent)
        text.place(x=0, y=0, width=565, height=130)
        self.vbar(text, 0, 0, 565, 130,parent)
        return text
    def __tk_list_box_ljts4yka(self,parent):
        lb = Listbox(parent)
        
        lb.place(x=0, y=0, width=565, height=130)
        return lb
    def __tk_list_box_ljttk802(self,parent):
        lb = Listbox(parent)
        
        lb.place(x=0, y=0, width=565, height=130)
        return lb
    def __tk_label_frame_ljtrrw77(self,parent):
        frame = LabelFrame(parent,text="编译区",)
        frame.place(x=590, y=10, width=340, height=180)
        return frame
    def __tk_select_box_lk94nk8u(self,parent):
        cb = Combobox(parent, state="readonly", )
        cb['values'] = ("")
        cb.place(x=120, y=0, width=210, height=25)
        return cb
    def __tk_check_button_lk9fh8v8(self,parent):
        cb = Checkbutton(parent,text="指定顶层模块：-s",)
        cb.place(x=10, y=60, width=140, height=25)
        return cb
    def __tk_check_button_lk9fkk4k(self,parent):
        cb = Checkbutton(parent,text="导入VPI模块 -m -L",)
        cb.place(x=10, y=120, width=200, height=25)
        return cb
    def __tk_label_lk9fmrcl_左对齐文本(self,parent):
        label = Label(parent,text="设置HDL语法：",anchor="w", )
        label.place(x=10, y=0, width=110, height=25)
        return label
    def __tk_input_lk9foc7k(self,parent):
        ipt = Entry(parent, )
        ipt.place(x=160, y=60, width=170, height=25)
        return ipt
    def __tk_button_编译iverilog_按钮(self,parent):
        btn = Button(parent, text="编译", takefocus=False,)
        btn.place(x=250, y=120, width=80, height=30)
        return btn
    def __tk_label_lk9g07nn_左对齐文本(self,parent):
        label = Label(parent,text="建议使用SystemVerilog2005",anchor="w", )
        label.place(x=10, y=30, width=300, height=20)
        return label
    def __tk_label_lk9g339b_左对齐文本(self,parent):
        label = Label(parent,text="未勾选则由编译器自动判断",anchor="w", )
        label.place(x=10, y=90, width=300, height=20)
        return label
    def __tk_label_frame_ljtrs9ce(self,parent):
        frame = LabelFrame(parent,text="仿真区",)
        frame.place(x=590, y=200, width=340, height=180)
        return frame
    def __tk_check_button_lk9ftgsl(self,parent):
        cb = Checkbutton(parent,text="禁用交互模式 -n",)
        cb.place(x=10, y=0, width=150, height=25)
        return cb
    def __tk_check_button_lk9fvkel(self,parent):
        cb = Checkbutton(parent,text="打印log文件 -l",)
        cb.place(x=170, y=0, width=150, height=25)
        return cb
    def __tk_label_lk9fwn4s_左对齐文本(self,parent):
        label = Label(parent,text="设置波形文件格式：",anchor="w", )
        label.place(x=10, y=30, width=130, height=25)
        return label
    def __tk_select_box_lk9fxdq1(self,parent):
        cb = Combobox(parent, state="readonly", )
        cb['values'] = ("列表框","Python","Tkinter Helper")
        cb.place(x=140, y=30, width=190, height=25)
        return cb
    def __tk_input_lk9fysyb(self,parent):
        ipt = Entry(parent, )
        ipt.place(x=140, y=60, width=190, height=25)
        return ipt
    def __tk_label_lk9g5lwz_左对齐文本(self,parent):
        label = Label(parent,text="默认为dump，此设置会被$dumpfile系统任务覆盖",anchor="w", )
        label.place(x=10, y=90, width=330, height=20)
        return label
    def __tk_input_lk9g95q4(self,parent):
        ipt = Entry(parent, )
        ipt.place(x=140, y=120, width=80, height=25)
        return ipt
    def __tk_label_lk9g9bhx_左对齐文本(self,parent):
        label = Label(parent,text="最大仿真时间(秒)：",anchor="w", )
        label.place(x=10, y=120, width=130, height=25)
        return label
    def __tk_button_仿真vvp_按钮(self,parent):
        btn = Button(parent, text="仿真", takefocus=False,)
        btn.place(x=250, y=120, width=80, height=30)
        return btn
    def __tk_check_button_lk9hcd50(self,parent):
        cb = Checkbutton(parent,text="设置波形文件名：",)
        cb.place(x=10, y=60, width=130, height=25)
        return cb
    def __tk_button_查看波形_按钮(self,parent):
        btn = Button(parent, text="查看波形", takefocus=False,)
        btn.place(x=600, y=390, width=80, height=30)
        return btn
    def __tk_button_查看使用教程_按钮(self,parent):
        btn = Button(parent, text="查看使用教程", takefocus=False,)
        btn.place(x=490, y=10, width=90, height=30)
        return btn
    def __tk_button_清空3个信息框_按钮(self,parent):
        btn = Button(parent, text="清空", takefocus=False,)
        btn.place(x=520, y=370, width=60, height=30)
        return btn
    def __tk_button_打开工程目录_按钮(self,parent):
        btn = Button(parent, text="打开工程目录", takefocus=False,)
        btn.place(x=370, y=10, width=90, height=30)
        return btn
    def __tk_label_lk8cwqm2(self,parent):
        label = Label(parent,text="工程路径",anchor="center", )
        label.place(x=10, y=50, width=70, height=30)
        return label
    def __tk_button_导出脚本_按钮(self,parent):
        btn = Button(parent, text="导出脚本", takefocus=False,)
        btn.place(x=600, y=430, width=80, height=30)
        return btn
    def __tk_button_lk9gqgjy(self,parent):
        btn = Button(parent, text="清理工程目录", takefocus=False,)
        btn.place(x=600, y=470, width=80, height=30)
        return btn
class Win(WinGUI):
    def __init__(self):
        super().__init__()
        self.__event_bind()
    def 清空文本框(self,evt):
        print("<Button-1>事件未处理:",evt)
    def __event_bind(self):
        self.widget_dic["tk_button_清空3个信息框_按钮"].bind('<Button-1>',self.清空文本框)
        pass
if __name__ == "__main__":
    win = Win()
    win.mainloop()
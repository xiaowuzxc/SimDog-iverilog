# 使用VPI
Icarus Verilog在Verilog上实现了PLI 2.0 API的子集。这允许程序员编写和Verilog仿真器连接的C代码，用于执行与直接Verilog不实际的任务(不知道咋翻译)。许多Verilog设计者，尤其是那些只将Verilog作为综合工具的设计者，可以安全地忽略PLI的整个问题（以及本章），但希望将仿真器与外部世界连接的设计者无法脱离VPI。  
本文剩下的部分默认您对C编程、Verilog PLI和系统上的编译器有一些了解。在大多数情况下，Icarus Verilog默认GNU编译器是您正在使用的编译器，因此下面的提示和说明反映了这一点。如果你不是一个C程序员，或者没有规划任何的VPI模块，你可以跳过整篇文章。底部有参考资料，可获取更多的基础知识。  

## 它如何工作
VPI模块是编译生成的可加载对象代码，运行时可根据使用者的请求来加载这些代码。命令vvp使用`-m`开关来定位和加载模块。例如，要加载`sample.vpi`模块：  
```
% vvp -msample foo.vvp
```
vvp运行时在执行任何仿真之前，甚至在编译vvp代码之前，首先要加载模块。部分加载包括调用初始化例程。这些例程向运行时注册模块实现的所有系统任务和功能。一旦完成，运行时加载程序就可以将设计中被调用的系统任务的名称与VPI模块中的实现进行匹配。(译者注：简而言之，仿真前需要先处理VPI模块，把VPI模块和系统函数关联起来)  
（有一个特殊模块system.vpi，它总是被加载以提供核心的系统任务。）  
仿真器运行时（`vvp`程序）从加载的模块中查找符号`vlog_startup_routines`以获取新加载模块的句柄。该表由模块作者提供并编译到模块中，是一个以null结尾的函数指针表。仿真器按顺序调用表中的每个函数。以下是一个C示例表：  
```
void (*vlog_startup_routines[])() = {
   hello_register,
   0
};
```
请注意，`vlog_startup_routines`表是一个函数指针数组，最后一个指针为0表示结束。如果需要，程序员可以把很多模块放到表中的启动函数。  
在启动表中收集启动函数的目的是声明模块提供的系统任务和功能。模块可根据需求实现的任意数量的任务/功能，因此模块可以合法地被称为系统任务和功能库。  

## 编译VPI模块
如果要编译/链接VPI模块并与Icarus Verilog一起使用，您必须编译模块的所有源文件，就像为DLL或共享对象编译一样。对于Linux下的gcc，需要使用`-fpic`标志进行编译。然后，模块与vpi库链接在一起，如下所示：  
```
% gcc -c -fpic hello.c
% gcc -shared -o hello.vpi hello.o -lvpi
```
这步默认`vpi_user.h头文件和libvpi.a库文件已安装在您的系统上，以便gcc可以找到它们。在Linux和UNIX系统下通常是这种情况。在所有支持的系统上，有一种更简单、首选的方法，是使用单个命令：  
```
% iverilog-vpi hello.c
```
`iverilog vpi`命令将vpi模块的源文件作为命令参数，使用适当的编译器标志对其进行编译，并将其链接到具有所需的任何系统特定库和链接器标志的vpi模块中。这个简单的命令使得`hello.vpi`模块不容易出问题。  

## 一个实例
让我们尝试一个完整的、有效的实例。将下面的C代码放入hello.c文件中：  
```
# include  <vpi_user.h>

static int hello_compiletf(char*user_data)
{
      return 0;
}

static int hello_calltf(char*user_data)
{
      vpi_printf("Hello, World!\n");
      return 0;
}

void hello_register()
{
      s_vpi_systf_data tf_data;

      tf_data.type      = vpiSysTask;
      tf_data.tfname    = "$hello";
      tf_data.calltf    = hello_calltf;
      tf_data.compiletf = hello_compiletf;
      tf_data.sizetf    = 0;
      tf_data.user_data = 0;
      vpi_register_systf(&tf_data);
}

void (*vlog_startup_routines[])() = {
    hello_register,
    0
};
```
并将下面的Verilog代码放入hello.v中：  
```
module main;
  initial $hello;
endmodule
```
接下来，按照以下步骤编译并执行代码：
```
% iverilog-vpi hello.c
% iverilog -ohello.vvp hello.v
% vvp -M. -mhello hello.vvp
Hello, World!
```
本实例中，编译和链接步骤合并成`iverilog-vpi`命令。然后，`iverilog`命令将`hello.v`Verilog源文件编译为`hello.vvp`程序。接下来，`vvp`命令演示使用`-M`和`-m`标志来指定vpi模块搜索目录和vpi模块名称。具体来说，它们告诉`vvp`命令在哪里可以找到我们刚刚编译的vpi模块。  
如上所述执行`vvp`命令时，会加载从当前工作目录找到的`hello.vpi`模块。加载模块时，扫描vlog_startup_routines表，并执行`hello_register`函数。`hello_register`函数反过来告诉`vvp`此模块中包含的系统任务。  
加载完所有模块后，便加载`hello.vvp`设计文件，并将`$hello`系统任务调用与模块声明的版本相匹配。当`vvp`编译`hello.vvp`源代码时，对`$hello`的任何调用都会引用`compiletf`函数。此函数在编译时调用，可用于检查系统任务或函数的参数。它可以像这样空着，也可以完全忽略。`compiletf`函数可以通过在编译时收集参数检查来改善性能，因此无需每次运行系统任务都进行检查，从而节省一些总体执行时间。  
当运行时执行对hello系统任务的调用时，在加载的模块中调用`hello_calltf`函数，从而生成输出。当Verilog代码实际执行系统任务时，会在运行时调用`calltf`函数。这是任务的活动代码所属的位置。  

## 系统函数返回类型
Icarus Verilog支持系统功能和系统任务，但也存在一个复杂的问题。请注意，编译的模块是由`vvp`程序加载的。这基本上不是问题，但表达式的elaboration需要跟踪类型，因此主编译器需要知道函数的返回类型。  
从Icarus Verilog v11开始，解决方案非常简单。用户的VPI模块名称和位置可以通过`iverilog -m`和`-L`标志与`IVERILOG_VPI_MODULE_PATH`环境变量传递给编译器。编译器将加载并分析指定的模块，以自动确定每个函数的返回类型。编译器还会自动把指定的模块名称和位置传递给`vvp`程序，这样就不需要在`vvp`命令行上再次指定它们。(译者注：v11及以上版本看到这里就行了)    
对于v11之前的Icarus Verilog版本，该解决方案要求模块的开发人员用编译器可以读取的形式包含表。系统功能表文件包含这些信息。一个简单的例子如下：    
```
# 一些常见函数的sft声明示例
$random      vpiSysFuncInt
$bitstoreal  vpiSysFuncReal
$realtobits  vpiSysFuncSized 64 unsigned
```
这展示了文件的格式和支持类型。每一行都包含一个注释（以`#`开头）或单个函数的类型声明。声明从系统函数的名称（包括前导的`$`）开始，到类型结束。支持的类型包括：  
- vpiSysFuncInt  
- vpiSysFuncReal  
- vpiSysFuncSized <wid> <signed|unsigned>  

任何在SFT文件中没有显式声明类型的函数都被隐式地视为`vpiSysFuncSized 32 unsigned`。  
模块作者提供了一个声明所有函数返回类型的`.sft`，以及作为模块的`.vpi`文件。例如，如果文件名为`example.sft`，则将其传递到`iverilog`命令行或命令文件中，就像它是一个普通源文件一样。  

## Cadence PLI模块
(自行研究)
With the cadpli module, Icarus Verilog is able to load PLI1 applications that were compiled and linked to be dynamic loaded by Verilog-XL or NC-Verilog. This allows Icarus Verilog users to run third-party modules that were compiled to interface with XL or NC. Obviously, this only works on the operating system that the PLI application was compiled to run on. For example, a Linux module can only be loaded and run under Linux. In addition, a 64-bit version of vvp can only load 64-bit PLI1 applications, etc.   
Icarus Verilog uses an interface module, the `cadpli` module, to connect the worlds. This module is installed with Icarus Verilog, and is invoked by the usual -m flag to iverilog or vvp. This module in turn scans the extended arguments, looking for -cadpli= arguments. The latter specify the share object and bootstrap function for running the module. For example, to run the module product.so, that has the bootstrap function `my_boot`:  
```
% vvp -mcadpli a.out -cadpli=./product.so:my_boot  
```
The `-mcadpli` argument causes vvp to load the cadpli.vpl library module. This activates the -cadpli= argument interpreter. The -cadpli=<module>:<boot_func> argument, then, causes vvp, through the cadpli module, to load the loadable PLI application, invoke the my_boot function to get a veriusertfs table, and scan that table to register the system tasks and functions exported by that object. The format of the -cadpli= extended argument is essentially the same as the +loadpli1= argument to Verilog-XL.  
The integration from this point is seamless. The PLI application hardly knows that it is being invoked by Icarus Verilog instead of Verilog-XL, so operates as it would otherwise.  

## 参考资料
由于以上内容仅仅解释了如何让PLI/VPI与Icarus Verilog协同工作，因此下面列举了一些参考资料，以帮助了解PLI/VPI的常用知识。  
- Principles of Verilog PLI by Swapnajit Mittra. ISBN 0-7923-8477-6  
- The Verilog PLI Handbook by Stuart Sutherland. ISBN 0-7923-8489-X  

















# GUI绘制技巧
python+tk  
https://www.pytk.net/tkinter-helper/  
https://steveicarus.github.io/iverilog/  
https://iverilog.fandom.com/wiki/Main_Page
标签，按钮，输入框，文本框，列表框，下拉框，复选框  

### 文本框禁用输入
```
ipt = Entry(parent, state=DISABLED)
text = Text(parent, state=DISABLED)
```

### 带滚动条的列表
更好的竖滚动条，`self.vbar`内部参数参考上方`place`  
```
    def scrollbar_autohide(self,vbar, hbar, widget):
        """自动隐藏滚动条"""
        def show():
            if vbar: vbar.pack(side=RIGHT, fill=Y)
            if hbar: hbar.pack(side=BOTTOM, fill=X)
        def hide():
            if vbar: vbar.pack_forget()
            if hbar: hbar.pack_forget()
        hide()
        widget.bind("<Motion>", lambda e: show())
        if vbar: vbar.bind("<Motion>", lambda e: show())
        if vbar: vbar.bind("<Leave>", lambda e: hide())
        if hbar: hbar.bind("<Motion>", lambda e: show())
        if hbar: hbar.bind("<Leave>", lambda e: hide())
        widget.bind("<Leave>", lambda e: hide())
。。。。。。
        lb.place(x=0, y=30, width=565, height=220)
        #竖滚动条放外面
        vbar = Scrollbar(lb, orient=VERTICAL, command=lb.yview)#进父容器
        vbar.pack(side=RIGHT, fill=Y)#放置位置，side=[TOP,BOTTOM,RIGHT,LEFT]
        lb.configure(yscrollcommand=vbar.set)#关联v-y轴
        #横滚动条塞进内部
        hbar = Scrollbar(lb, orient=HORIZONTAL, command=lb.xview)#x滚动条，父容器lb，水平，和x轴关联
        hbar.pack(side=BOTTOM, fill=X)#放置位置，side=[TOP,BOTTOM,RIGHT,LEFT]
        lb.configure(xscrollcommand=hbar.set)#关联h-x轴
        self.scrollbar_autohide(vbar, hbar, lb)
        return lb
```


### 初始化GUI
在`def __init__`函数最下方进行初始化操作  
```
class Win(WinGUI):
    def __init__(self):
        super().__init__()
        self.__event_bind()
        self.widget_dic["tk_text_信息文本框"].insert(END,"114514")#初始化信息
```
### GUI内绑定变量
在`class WinGUI(Tk)`定义变量`复选框val`  
```
class WinGUI(Tk):
    widget_dic: Dict[str, Widget] = {}
    复选框val=0#给外面的函数用
    def __init__(self):
        super().__init__()
        self.__win()
        self.widget_dic["tk_input_单行输入输出"] = self.__tk_input_单行输入输出(self)
        self.widget_dic["tk_label_输入区域"] = self.__tk_label_输入区域(self)
        self.widget_dic["tk_button_按键0"] = self.__tk_button_按键0(self)
        self.widget_dic["tk_check_button_复选框0"] = self.__tk_check_button_复选框0(self)[0]
        self.复选框val = self.__tk_check_button_复选框0(self)[1]#绑定量给复选框val
...
    def __tk_check_button_复选框0(self,parent):
        绑定量=BooleanVar()#tk布尔型
        cb = Checkbutton(parent,text="点一下",variable = 绑定量)#绑定
        cb.place(x=80, y=60, width=80, height=30)
        return cb,绑定量#元组形式返回
```
在`class Win(WinGUI):`的使用方式为
```
print(type(self.复选框val))
复选框=self.复选框val.get()#通过X.get读取复选框
```
### Label标签左对齐
```
def __tk_label_lk9g9bhx_左对齐文本(self,parent):
    label = Label(parent,text="最大仿真时间(秒)：",anchor="center", )
    label.place(x=10, y=120, width=130, height=25)
    return label
```
其中，`anchor`的参数改为`w`，即`anchor="w"`  

### Text文本框中显示不同颜色的字
```
# 创建一个文本框
text = Text(window, height=10, width=50)
text.pack()
# 设置文本的字体颜色为红色和蓝色并附上标签
text.tag_configure('red', foreground='red')
text.tag_configure('blue', foreground='blue')
# 在文本框中插入一些文本，并应用不同的标签
text.insert(END, 'Hello, ', 'red')
text.insert(END, 'World!', 'blue')
# 运行窗口
window.mainloop()
```
### 仿真策略

####
检测系统环境
```
import sys
if sys.platform.startswith('win'):
    print('win32?当前环境为Windows')
elif sys.platform.startswith('linux'):
    print('当前环境为Linux')
else:
    print('未知')
```


#### 仿真未超时
使用`import subprocess`  
```
a=subprocess.run('vvp -n -l log.txt tb',stdout=subprocess.PIPE,encoding='utf-8',timeout=3)
```
`stdout=subprocess.PIPE`表示接受输出  
`encoding='utf-8'`表示输出按照格式来，而不是bytes  
`timeout=3`表示3秒超时  
然后`print(a.stdout)`可以显示仿真输出的信息  

#### 仿真超时
执行指令  
```
a=subprocess.run('vvp -n -l log.txt tb',stdout=subprocess.PIPE,encoding='utf-8',timeout=3)
```
如果超时了，则变量a不存在，通过  
```
if 'a' in locals():
    print('变量a存在')
else:
    print('变量a不存在')
```
就能判断，超时还是没有超时  

如果没超时，把`a.stdout`显示在终端里  
如果超时，弹出消息，提示使用外置脚本启动  

#### 使用Popen
```
a=subprocess.Popen('vvp -n tb.out',stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding='utf-8')
a.poll()返回None则进程未结束，返回数字则进程已结束，判断方式 (a.poll() is None) -> True
a.kill()杀死进程
a.communicate(timeout=114)和子进程交互，发送和读取数据。
```

### 保存设置
使用一个字典，把所有东西都存进来
以文本形式保存
以文本形式读出
通过aaa=eval(字典)完成转换

### 弹出消息
```
from tkinter import messagebox
.....
messagebox.showwarning('标题', '内容')
```

## iverilog相关
### 编译VPI
```
iverilog-vpi [options] sourcefile...
iverilog-vpi --name=YYY XXX.c
```
通常，输出VPI模块将以传递给命令的第一个源文件命名。此标志设置输出vpi模块的名称（不带.vpi后缀）。
多个.c文件编译输出一个`YYY.vpi`

### 编译区
#### 多选框
添加VPI  
指定顶层模块  
#### 下拉框
语法选择  
- SV2005-g2005-sv(推荐)
- Verilog05-g2005
- Verilog01-g2001
- Verilog95-g1995

#### 输入框
设置-s顶层模块  

### 仿真区
#### 多选框
-n禁用交互模式   
-l打印log文件

#### 下拉框
波形文件格式  
- VCD(推荐)
- fst
- lxt
- lxt2
- 禁用

#### 输入框
最大仿真时间（默认10s）  
波形文件名（无$dumpfile）  

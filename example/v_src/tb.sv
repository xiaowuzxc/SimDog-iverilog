`timescale 1ns/100ps
module tb(); 
logic clk;

initial begin
    clk = 1;
    $display("Hello Loop");
    forever begin
        #1 clk = ~clk;
    end
end

initial begin
    #10;
    $display("init 2");
end

initial begin
    $dumpfile("tb.lxt");  //生成lxt的文件名称
    $dumpvars(0,tb);   //tb中实例化的仿真目标实例名称   
end

endmodule
